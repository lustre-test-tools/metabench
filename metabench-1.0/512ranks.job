#!/bin/bash
#PBS -l mppwidth=512
#PBS -l mppnppn=16
#PBS -l walltime=0:10:00
#PBS -N Metabench512
#PBS -j oe
#PBS -S /bin/bash

# ======================================================================
# DEFINITIONS:
#      SYSNAME    =  name of cluster
#      NPROC      =  number of processes
#      NODES      =  number of cluster nodes
#      PPN        =  number of processors per node
#      SRC_DIR    =  location of source code directory
#      PROGRAM    =  location of compiled binary for this system
#      DICTIONARY = location of dictionary for filename generation
#      RUN_DIR    = top level directory where jobs should be run
#      WORK_DIR   = subdir of RUN_DIR where job will be run
#      RUNOPTS    =  Metabench run options
#                    -T N   =>  timed create N files by one proc
#                    -c N   =>  procs create N files in sep dirs
#                    -C N   =>  procs create N files in same dir
#                    -S     =>  file stat test
#                    -U     =>  file update test
#                    -A SZ  =>  file append test with SZ bytes
#                    -D     =>  file delete test
#                    -r     =>  rotate dirs between ops
# ======================================================================

SYSNAME=hera
NPROC=512
PPN=16
NODES=$(($NPROC/$PPN))
FILES=100

# fix directory issues
# HOME=`cd -P $HOME; pwd`
SCRATCH=/lus/sonexion/$LOGNAME

SRC_DIR=$SCRATCH/Metabench
PROGRAM=$SRC_DIR/src/metabench
DICTIONARY=$SRC_DIR/dictionary
RUN_DIR=$SCRATCH/Metabench/RUN
WORK_DIR=run_${NPROC}.${FILES}f
# ======================================================================

echo SYSNAME=$SYSNAME
echo NPROC=$NPROC
echo NODES=$NODES
echo PPN=$PPN
echo SRC_DIR=$SRC_DIR
echo PROGRAM=$PROGRAM
echo DICTIONARY=$DICTIONARY
echo RUN_DIR=$RUN_DIR
echo WORK_DIR=$WORK_DIR

# Create the run directory if necessary
rm -rf $RUN_DIR
mkdir -p $RUN_DIR
cd $RUN_DIR

echo ======================================================================
echo RUN DIRECTORY `pwd`
echo RUN STARTING AT  `date '+ %Y-%m-0 %H:%M:%S'`
echo ======================================================================

# run the file create/delete tests with 100 files per process in separte directories
PARGS="-c ${FILES} -D "
echo "Running [aprun -n $NPROC -N $PPN $PROGRAM -p $DICTIONARY -w $WORK_DIR $PARGS]"
aprun -n $NPROC -N $PPN $PROGRAM -p $DICTIONARY -w $WORK_DIR $PARGS

# run the file create/delete tests with 100 files per process in the same directory
PARGS="-C ${FILES} -D "
echo "Running [aprun -n $NPROC -N $PPN $PROGRAM -p $DICTIONARY -w $WORK_DIR $PARGS]"
aprun -n $NPROC -N $PPN $PROGRAM -p $DICTIONARY -w $WORK_DIR $PARGS

echo ======================================================================
echo RUN STOPPING AT  `date '+ %Y-%m-0 %H:%M:%S'`
echo ======================================================================

