#!/bin/bash

for NODES in 1 2 4 8 12 16 20; do
	NPROC=$(( ${NODES} * 16 ))
	script -c "NPROC=${NPROC} bash ./Nranks.job" Nranks.${NODES}
	chmod 444 Nranks.${NODES}
done
