#include "metabench.h"
#include <stdarg.h>

static unsigned long rseed = 0;
static int test_namegen_count = 1000000;
static int testnamegen = 0;
static int max_open_files = 64*1024;
int debug = 0;
int verbose = 0;
int report_freq = 1000;
int rotate_dirs = 0;
int prealloc_filenames = 0;
int delay_create = 0;
int oper_loop_count = 1;
int *open_files = NULL;
unsigned long timed_create_nfile = 0;
unsigned long onedir_nfilepp = 0;
unsigned long multidir_nfilepp = 0;
char *workdir_root = DEFAULT_PATH;
long int  file_append_size = 4096;
int do_cleanup = 1;

static char *dictionary_path = DEFAULT_DICTIONARY;

mode_t dir_mode = DEFAULT_DIR_MODE;
mode_t file_mode = DEFAULT_FILE_MODE;

int num_proc = 1;
int my_rank = 0;
int proc0 = 0;
int page_size;

FILE *ofl = NULL;
char *testname = NULL;

static char hostname[MAX_HOSTNAME];
static string_arr_t all_hostname;
static int  *host_ids = NULL;
static int  num_hosts = 0;
static int  my_hostid = -1;

static unsigned long  *count_buf = NULL;
static double  *double_buf = NULL;
static double  *rate = NULL;

/* global constants for row communicator */
MPI_Comm  row_comm;
int       row_comm_size = 0;
int       my_row_rank   = -1;
int       test_row_communicator = 0;

char* MetadataNames[NUM_OPERATIONS] = {
    "STAT",
    "OPEN",
    "CLOSE",
    "UTIME",
    "APPEND",
    "DELETE"
};
int run_test[NUM_OPERATIONS] = { 0, };

/* prototypes of local functions */
void time_file_creation(char* workdir, int proc_id, int filecount, int rpt_freq);
void par_create_multidir(char *workdir, unsigned long filecountpp, char* mydir);
void par_create_onedir(char *workdir, unsigned long filecountpp);
void par_oper_multidir(char *workdir, metaop_t oper);
void par_oper_onedir(char *workdir, unsigned long nfilepp, metaop_t oper);
void compute_stats(int n, double *data, double *mean, double *sigma);
void print_results(FILE *out, unsigned long *files, double *dt, double elapsed_dt);
void create_shift_comm(void);
void rotate_strings(char *string, int str_len);
void print_string_all(char *str, string_arr_t *all_strings);
void row_comm_test(void);
void parse_args(int argc, char *argv[] );

int main(int argc, char* argv[]) 
{
    int i, len;
    char *p;
    MPI_Status status;
    int np, nproc;
    time_t gmtime;
    struct tm cur_time;
    char outfilename[MAXPATHLEN];
    char tmpdir[MAXPATHLEN];
    char prefix[MAXPATHLEN];
    metaop_t oper;
    double   ntime;
    int      dictionary_files = 10000;

    /* get the page size for this system */
    page_size = sysconf(_SC_PAGE_SIZE);

    /* determine the hostname of this processor */
    if (gethostname(hostname,MAX_HOSTNAME) ) {
	perror("Unable to get hostname");
	strcpy(hostname,"UNKNOWN");
    }

#define NAME_TAG 101
    MPI_SAFE(MPI_Init(&argc,&argv));
    MPI_SAFE(MPI_Comm_size(MPI_COMM_WORLD,&num_proc));
    MPI_SAFE(MPI_Comm_rank(MPI_COMM_WORLD,&my_rank));

    /* collect command line args */
    parse_args(argc,argv);

    /* printf("%d/%d checking in\n",my_rank,num_proc); */

    /* proc 0 is the only one that writes messages to stdout or the
     * report file.  Open the files here and allocate boffers
     * only proc 0 will need for gather/scatter ops.
     */
    if (my_rank == proc0) {

        /* allocate buffers used to gather stats from all procs */
        count_buf = (unsigned long*)malloc(num_proc * sizeof(unsigned long));
        if (count_buf == NULL) {
            fatal("Allocation of count_buf array failed");
        }
        double_buf = (double*)malloc(num_proc * sizeof(double));
        if (double_buf == NULL) {
            fatal("Allocation of double_buf array failed");
        }
        rate = (double*)malloc(num_proc * sizeof(double));
        if (rate == NULL) {
            fatal("Allocation of rate array failed");
        }

	/* construct name of output file */
	time(&gmtime);
	localtime_r(&gmtime,&cur_time);

	if (testname == NULL) {
	    /* just write to stdout */
	    ofl = stdout;
	} else {
	    /* construct a name with current time and date */
	    sprintf(outfilename,"%s_%04d%02d%02d_%02d%02d%02d",
		    testname,cur_time.tm_year+1900,
		    cur_time.tm_mon+1,cur_time.tm_mday,
		    cur_time.tm_hour,cur_time.tm_min,cur_time.tm_sec);
	    if ( (ofl = fopen(outfilename,"w")) == NULL) {
		fatal("unable to open file %s",outfilename);
	    }
	}

	fprintf(ofl,"Metadata Test %s on %02d/%02d/%04d at %02d:%02d:%02d\n\n",
		(testname == NULL ? "<no-name>" : testname),
		cur_time.tm_mon+1,cur_time.tm_mday,
		cur_time.tm_year+1900,cur_time.tm_hour,
		cur_time.tm_min,cur_time.tm_sec);
    }
    
    /* exchange hostnames and organize processes into an array where the
     * columns contain all the ranks of processes on a particular host
     * and the rows contain at most one process on each host.
     * From this we construct communicators, one for each row in the
     * array.  Globally, there are as many communicators as rows in
     * the array, and each process belongs to exactly one.
     * We use the communicator set to exchange directory names.
     * If process X creates a directory and populates it with files,
     * then it will send the name of this dir to another process in its
     * row comm, that process will perform operations (stat, update, delete)
     * on the files in the dir.  This way, files created by one process will
     * be operated on by a process in a distinct OS image.  So, no process
     * will have an advantage by caching metadata, all processes will be at
     * the same disadvantage.
     */
    create_shift_comm();

    /* init random number generator */
    if (rseed == 0) {
        /* it was not specified on the command line, gen one */
        unsigned int rseed1 = ( (((unsigned int) clock()) & 0xFFFF) << 16 );
        unsigned int rseed2 = (unsigned int) my_rank;
        unsigned int rseed3 = (rseed1 | rseed2);
        srandom(rseed3);
        rseed = rseed3;
        /*
        fprintf(stderr,"Rank %4d: Random seed = %lu\n",my_rank,rseed);
        */
    }
    if (verbose && (my_rank == proc0)) {
        fprintf(ofl,"Rank %3d process using seed %lu\n",my_rank,rseed);
    }

    /* hook to test name generation algorithm */
    if (testnamegen) {
        init_dictionary(dictionary_path, test_namegen_count);
        test_namegen(test_namegen_count);
        MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
	exit(0);
    }

    /* Compute max files per process we will need to generate and then
       init the dictionary with this value.
    */
    if (timed_create_nfile > dictionary_files)
        dictionary_files = timed_create_nfile;
    if (onedir_nfilepp > dictionary_files)
        dictionary_files = onedir_nfilepp;
    if (multidir_nfilepp > dictionary_files)
        dictionary_files = multidir_nfilepp;
    init_dictionary(dictionary_path, dictionary_files);


    /* insure working directory exists */
    if (my_rank == proc0) {
	create_path(workdir_root);
	if (verbose) {
	    printf("Created path [%s]\n",workdir_root);
	}
    }

    /* wait for everyone before starting tests */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    /* run timed file creation test by a single dir */
    if (timed_create_nfile > 0) {
	int proc_id = num_proc-1;
        char workdir[MAXPATHLEN];

        /* reset the dictionary counters */
        reset_dictionary();

	/* time how long it takes for highest rank process to create files */
	sprintf(prefix,"TIME_CREATE_%06d",proc_id);
	create_unique_dir(workdir_root,prefix,workdir,MAXPATHLEN,
			  MPI_COMM_WORLD);
	time_file_creation(workdir,proc_id,timed_create_nfile,
			   report_freq);
	if (do_cleanup && (my_rank == proc_id)) {
	    remove_dir(workdir);
	}
    }

    /* run the parallel tests where each process works in a seperate dir */
    if (multidir_nfilepp > 0) {
        char workdir[MAXPATHLEN];
        char mydir[MAXPATHLEN];

        /* reset the dictionary counters */
        reset_dictionary();

	/* have each proc create files in distinct dirs */
	sprintf(prefix,"CREATE_MD_%06d",num_proc);
	create_unique_dir(workdir_root,prefix,workdir,MAXPATHLEN,
			  MPI_COMM_WORLD);
	par_create_multidir(workdir,multidir_nfilepp,mydir);

        /* Now perform the operations as many times as requested (except DELETE) */
        for (int itercnt = 0; itercnt < oper_loop_count; itercnt++) {
            for (oper = 0; oper < NUM_OPERATIONS; oper++) {

                if (oper == DELETE_OP) {
                    /* we only do this once, at the end */
                    continue;
                }

                if (run_test[oper]) {

                    if (rotate_dirs) {
                        rotate_strings(mydir,MAXPATHLEN);
                    }

                    /* run the operation */
                    par_oper_multidir(mydir,oper);

                    if (oper == OPEN_OP) {
                        /* must do the corresponding close op now */
                        if (rotate_dirs) {
                            rotate_strings(mydir,MAXPATHLEN);
                        }
                        par_oper_multidir(mydir,CLOSE_OP);
                    }
                }

            }
        }
        if (run_test[DELETE_OP]) {
            if (rotate_dirs) {
                rotate_strings(mydir,MAXPATHLEN);
            }
            par_oper_multidir(mydir,DELETE_OP);
        }

        /* may be no-op if one of the timing ops above was to delete */
	if (do_cleanup && (my_rank == proc0)) {
	    remove_dir(workdir);
	}
    }


    /* parallel creates and ops in a single dir */
    if (onedir_nfilepp > 0) {
        char workdir[MAXPATHLEN];

        /* reset the dictionary counters since will be generating filenames
         * for a different directory
         */
        reset_dictionary();

	/*  Have all processes create files in the same directory.
	 */
	sprintf(prefix,"CREATE_ONE_%06d",num_proc);
	create_unique_dir(workdir_root,prefix,workdir,MAXPATHLEN,
			  MPI_COMM_WORLD);
	par_create_onedir(workdir,onedir_nfilepp);

        /* Now perform the operations as many times as requested (except DELETE) */
        for (int itercnt = 0; itercnt < oper_loop_count; itercnt++) {
            /* now, do the operations */
            for (oper = 0; oper < NUM_OPERATIONS; oper++) {

                if (oper == DELETE_OP) {
                    /* we only do this once, at the end */
                    continue;
                }

                if (run_test[oper]) {
                    /* run the operation */
                    par_oper_onedir(workdir,onedir_nfilepp,oper);

                    if (oper == OPEN_OP) {
                        /* must do the corresponding close op now */
                        par_oper_onedir(workdir,onedir_nfilepp,CLOSE_OP);
                    }
                }

            }
        }
        if (run_test[DELETE_OP]) {
            par_oper_onedir(workdir,onedir_nfilepp,DELETE_OP);
        }

        /* may be no-op if one of the timing ops above was to delete */
	if (do_cleanup && (my_rank == proc0)) {
	    remove_dir(workdir);
	}
    }
        /* Now run any of the stat/utime/append/delete timing tests
         * in this same directory
         */
        for (oper = 0; oper < NUM_OPERATIONS; oper++) {
            if (run_test[oper]) {
                int itercnt;
                int loopcnt = oper_loop_count;
                if (oper == DELETE_OP) {
                    loopcnt = 1;
                }
                for (itercnt = 0; itercnt < loopcnt; itercnt++) {
                }
            }
        }


    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    /* destroy communicators */
    MPI_SAFE(MPI_Comm_free(&row_comm));

    if (my_rank == proc0) {
	fclose(ofl);
    }

    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    MPI_SAFE(MPI_Finalize());

    return 0;
}


/* --------------------------------------------------------------------
 * Parallel file creation in a disjoint series of directories.
 *
 * In this case, we want to compute the aggregate rate at which
 * files can be created without directory lock contention by
 * the parallel tasks.
 *
 * Each task creates its own directory (mydir) then populates it
 * with filecountpp empty files.
 * --------------------------------------------------------------------
 */
void par_create_multidir(char *workdir, unsigned long filecountpp, char* mydir)
{
    char oldpath[MAXPATHLEN];
    uint64_t usec = 0;
    uint64_t start_time, stop_time;
    double   elapsed, dt;
    int      use_node_id = 0;      /* dont have to create globally unique names */
    string_arr_t files;
    string_arr_t *files_ptr = NULL;

    /* save the old path */
    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }

    if (my_rank == proc0) {
	log_msg("Entering par_create_multidir to create %d files PP in each dir",filecountpp);
        log_msg("Current Directory = %s",oldpath);
    }


    /* have each process create its own dir */
    sprintf(mydir,"%s/proc_%06d",workdir,my_rank);
    if (mkdir(mydir,dir_mode)) {
        fatal("Cant create dir [%s] in [%s]",mydir,workdir);
    }

    if (chdir(mydir)) {
        fatal("Can't chmod to [%d] in [%s]",mydir,workdir);
    }
    if (my_rank == proc0) {
        log_msg("Proc0 running in %s",mydir);
    }
        
    if (prealloc_filenames) {
        files_ptr = &files;
        gen_n_filenames(filecountpp,MAXPATHLEN,use_node_id,&files);
    }

    /* wait for all proc to create their dirs */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    start_time = getMicrosecondTimeStamp();

    usec = create_n_files(filecountpp,use_node_id,files_ptr);
        
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    stop_time = getMicrosecondTimeStamp();
    if (chdir(oldpath)) {
        fatal("Can't chmod to %s from %s",oldpath,mydir);
    }

    if (prealloc_filenames) {
        free_str_array(&files);
    }

    /* send results to process 0 */
    dt = 1.0e-6*usec;
    MPI_SAFE(MPI_Gather(&filecountpp,1,MPI_UNSIGNED_LONG,
			count_buf,1,MPI_UNSIGNED_LONG,proc0,MPI_COMM_WORLD));
    MPI_SAFE(MPI_Gather(&dt,1,MPI_DOUBLE,double_buf,1,MPI_DOUBLE,
			proc0,MPI_COMM_WORLD));

    if (my_rank == proc0) {
	double elapsed = 1.0e-6*(stop_time - start_time);
	fprintf(ofl,"Parallel file creation by %d processes in separate directories\n",
		num_proc);
	print_results(ofl,count_buf,double_buf,elapsed);
	log_msg("Leaving par_create_multidir");
    }

    /* wait for all MPI processes at end */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

}


/* --------------------------------------------------------------------
 * Parallel file creation in a single directory
 *
 * In this function we compute the rate at which a file
 * system can create files within a single directory using
 * multiple creators.
 *
 * Note that this number should be compaired to the rate
 * at which a single process can create this number of files.
 * The performance difference will likely be lock contention
 * for the directory updates.
 * --------------------------------------------------------------------
 */
void par_create_onedir(char *workdir, unsigned long filecountpp)
{
    char oldpath[MAXPATHLEN];
    uint64_t usec = 0;
    uint64_t start_time, stop_time;
    double   dt;
    int use_node_id = 1;  /* create globally unique filenames */
    unsigned long filecount = 0;
    string_arr_t files;
    string_arr_t *files_ptr = NULL;

    /* How many files will be created?  */
    filecount = filecountpp * num_proc;

    if (my_rank == proc0) {
	log_msg("Entering par_create_onedir to make %lu files per process, %lu total",
                filecountpp,filecount);
    }

    /* save the old path */
    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }
    /* enter working directory */
    if (chdir(workdir)) {
	fatal("Can't chmod to [%s]",workdir);
    }

    if (prealloc_filenames) {
        files_ptr = &files;
        gen_n_filenames(filecountpp,MAXPATHLEN,use_node_id,&files);
    }

    /* wait for all proc to get here */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    start_time = getMicrosecondTimeStamp();

    usec = create_n_files(filecountpp,use_node_id,files_ptr);

    /* wait for all proc to complete */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    stop_time = getMicrosecondTimeStamp();
	
    if (chdir(oldpath)) {
	fatal("Can't chmod to [%s]",oldpath);
    }

    if (prealloc_filenames) {
        free_str_array(&files);
    }

    /* send results to process 0 */
    dt = 1.0e-6*usec;
    MPI_SAFE(MPI_Gather(&filecountpp,1,MPI_UNSIGNED_LONG,
			count_buf,1,MPI_UNSIGNED_LONG,proc0,MPI_COMM_WORLD));
    MPI_SAFE(MPI_Gather(&dt,1,MPI_DOUBLE,double_buf,1,MPI_DOUBLE,
			proc0,MPI_COMM_WORLD));

    if (my_rank == proc0) {
	double elapsed = 1.0e-6*(stop_time - start_time);
	fprintf(ofl,"Parallel file creation by %d processes in the same directory\n",
		num_proc);
	print_results(ofl,count_buf,double_buf,elapsed);
	log_msg("Leaving par_create_onedir");
    }

    /* wait for all MPI processes at end */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
}

/* --------------------------------------------------------------------
 * Parallel file opeation, each process in a different dir.
 *
 * Here, we create nproc dirs with a total of filecount files
 * and have each process measure the time it takes to perform
 * the given file operations on all the files in one of the dirs.
 *
 * The dir and files are created by a process 0 (proc0).
 * --------------------------------------------------------------------
 */
void par_oper_multidir(char *workdir, metaop_t oper)
{
    unsigned long count;
    char oldpath[MAXPATHLEN];
    int i;
    uint64_t start_time, stop_time, elapsed_stop;
    double   dt;
    DIR *dir;
    struct dirent *entry;
    struct stat st;
    char *oper_name = MetadataNames[oper];
    char *buf = NULL;
    char *baseptr = NULL;
    char c = (char)169;
    int fd;
    ssize_t written;
    ssize_t nbytes = (ssize_t)file_append_size;
    int use_node_id = 0;   /* dont need globally unique filenames */
    int my_fd;

    if (my_rank == proc0) {
	log_msg("Entering par_oper_multidir for %s operation with %d processes",
		oper_name,num_proc);
    }

    if (oper == APPEND_OP) {
	/* allocate the buffer used for the I/O operations
	 * and fill it with a bit pattern */
	buf = (char*)aligned_alloc(nbytes,page_size,(void**)&baseptr);
	assert(baseptr != NULL);
	memset(buf,c,nbytes);
    } else if (oper == OPEN_OP) {
        open_files = (int*)calloc(sizeof(int*),max_open_files);
        if (open_files == NULL) {
            fatal("Cant malloc open files container with %d entries",max_open_files);
        }
    } else if (oper == CLOSE_OP) {
        if (open_files == NULL) {
            fatal("No files open for CLOSE_OP operation");
        }
    }

    /* save the old path */
    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }
    /* enter working directory (assume it exists) */
    if (chdir(workdir)) {
	fatal("Process %d Can't chmod to [%s]",my_rank, workdir);
    }

    /* Open my directory */
    if ( (dir = opendir(".")) == NULL) {
        fatal("Process %d Cannot open dir [%s]",my_rank,workdir);
    }

    /* wait for all proc to get here before running operation */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    start_time = getMicrosecondTimeStamp();

    count = 0;
    while ( (entry = readdir(dir)) != NULL) {
            /* skip the . and .. directory entries */
            if (entry->d_name[0] == '.') {
                continue;
            }
	    switch (oper) {
	    case STAT_OP:
		if (lstat(entry->d_name,&st)) {
		    fatal("Unable to stat file [%s/%s]",workdir,entry->d_name);
		}
		break;
		
	    case OPEN_OP:
                if (count >= max_open_files) {
                    fatal("Tried to open more than limit of %d open files in par_oper_multidir",
                          max_open_files);
                }
                my_fd = open(entry->d_name, O_RDONLY);
		if (my_fd < 0) {
		    fatal("Unable to open file [%s/%s] for reading, err=%d",workdir,entry->d_name,errno);
		}
                open_files[count] = my_fd;
                my_fd = -1;
                break;

            case CLOSE_OP:
                my_fd = open_files[count];
                if (close(my_fd)) {
                    fatal("Unable to close file [%s/%s] errno = %d",workdir,entry->d_name,errno);
                }
		break;
		
	    case UTIME_OP:
		if (utime(entry->d_name,NULL)) {
		    fatal("Unable to utime file [%s/%s]",workdir,entry->d_name);
		}
		break;
	    
	    case DELETE_OP:
		if (lstat(entry->d_name,&st)) {
		    fatal("Unable to stat file [%s/%s] before unlink",workdir,entry->d_name);
		}
		/* only unlink regular files */
		if (!S_ISREG(st.st_mode)) {
		    continue;
		}
		if (unlink(entry->d_name)) {
		    fatal("Unable to unlink file [%s/%s]",workdir,entry->d_name);
		}
		break;
		
	    case APPEND_OP:
		if (lstat(entry->d_name,&st)) {
		    fatal("Unable to stat file [%s/%s]",workdir,entry->d_name);
		}
		/* only append to files! */
		if (!S_ISREG(st.st_mode)) {
		    continue;
		}
		if ( (fd = open(entry->d_name,O_WRONLY|O_APPEND)) == -1) {
		    fatal("Could not open %s in %s for append",
			  entry->d_name,workdir);
		}
		if ( (written = write(fd,buf,nbytes)) != nbytes ) {
		    if (written < 0) {
			fatal("unable to write %ld bytes to %s, fd %d, buf = %0x",
			      nbytes,entry->d_name,fd,(void*)buf);
		    }
		    warning("Short write: %ld bytes of %ld to %s",
			    written,nbytes,entry->d_name);
		}
		if (close(fd)) {
		    fatal("Unable to close %s in %s",entry->d_name,workdir);
		}
		break;

	    default:
		fatal("Unknown operation %d",(int)oper);
	    }
	    count++;
    }
    stop_time = getMicrosecondTimeStamp();
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    elapsed_stop = getMicrosecondTimeStamp();
	     
    if (closedir(dir)) {
        fatal("Unable to close directory [%s]",workdir);
    }

    /* get out of Dodge */
    if (chdir(oldpath)) {
	fatal("Can't chmod to [%s]",oldpath);
    }

    /* send results to process 0 */
    dt = 1.0e-6*(double)(stop_time - start_time);
    MPI_SAFE(MPI_Gather(&count,1,MPI_UNSIGNED_LONG,
			count_buf,1,MPI_UNSIGNED_LONG,proc0,MPI_COMM_WORLD));
    MPI_SAFE(MPI_Gather(&dt,1,MPI_DOUBLE,double_buf,1,MPI_DOUBLE,
			proc0,MPI_COMM_WORLD));

    if (my_rank == proc0) {
	double elapsed = 1.0e-6*(elapsed_stop - start_time);
	if (oper == APPEND_OP) {
	    fprintf(ofl,"File APPEND with %ld bytes by %d processes in different directories\n",
		    nbytes,num_proc);
	} else {
	    fprintf(ofl,"File %s by %d processes in different directories\n",
		    oper_name,num_proc);
	}
	print_results(ofl,count_buf,double_buf,elapsed);
	log_msg("Leaving par_oper_multidir for %s operation",oper_name);
    }

    if (oper == APPEND_OP) {
	free(baseptr);
    } else if (oper == CLOSE_OP) {
        free(open_files);
        open_files = NULL;
    }
    
    /* wait for all MPI processes at end */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
}

/* ==============================================================
 * We have multiple processes operate on different files in a 
 * single directory.
 *
 * in order to avoid caching issues, process 0 will create the
 * test directory and files, but not participate in the test.
 *
 * nproc = total number of processes doing the test (including 0)
 * nclient = number of process running the given metadata operation.
 * nclient == nproc-1 
 * ==============================================================
 */
void par_oper_onedir(char *workdir, unsigned long nfilepp, metaop_t oper)
{
    unsigned long my_start, pos, count;
    char oldpath[MAXPATHLEN];
    int i;
    uint64_t start_time, stop_time, elapsed_stop;
    double   dt;
    DIR *dir;
    struct dirent *entry;
    struct stat st;
    char *oper_name = MetadataNames[oper];
    char *buf = NULL;
    char *baseptr = NULL;
    char c = (char)169;
    int fd;
    ssize_t written;
    ssize_t nbytes = (ssize_t)file_append_size;
    int my_fd;

    if (my_rank == proc0) {
	log_msg("Entering par_oper_onedir for %s operation with %d processes",
		oper_name,num_proc);
    }
    if (oper == APPEND_OP) {
	/* allocate the buffer used for the I/O operations
	 * and fill it with a bit pattern */
	buf = (char*)aligned_alloc(nbytes,page_size,(void**)&baseptr);
	assert(baseptr != NULL);
	memset(buf,c,nbytes);
    } else if (oper == OPEN_OP) {
        open_files = (int*)calloc(sizeof(int*),max_open_files);
        if (open_files == NULL) {
            fatal("Cant malloc open files container with %d entries",max_open_files);
        }
    } else if (oper == CLOSE_OP) {
        if (open_files == NULL) {
            fatal("No files open for CLOSE_OP operation");
        }
    }

    
    /* save the old path */
    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }
    if (chdir(workdir)) {
	fatal("Can't chmod to [%s]",workdir);
    }

    /* wait until working dir is created */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    /* each process opens the directory and finds its starting place */
    {
	if ( (dir = opendir(".")) == NULL) {
	    fatal("Cannot open current dir [%s]",workdir);
	}
	
        my_start = nfilepp * my_rank;

	/* now move to the starting location for this process */
	pos = 0;
	while ( (entry = readdir(dir)) != NULL ) {
	    if (strcmp(entry->d_name,".") == 0)
		continue;
	    if (strcmp(entry->d_name,"..") == 0)
		continue;
	    if (pos == my_start) break;
	    pos++;
	}
	if (entry == NULL) {
	    fatal("Hit end of dir trying to start at location %d",my_start);
	}
    }

    /* wait for all procs to get to starting location */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    /* start timer and do the operations */
    start_time = getMicrosecondTimeStamp();

    count = 0;
    do {
	    switch (oper) {
	    case STAT_OP:
		if (lstat(entry->d_name,&st)) {
		    fatal("Unable to stat file [%s/%s]",workdir,entry->d_name);
		}
		break;

	    case OPEN_OP:
                if (count >= max_open_files) {
                    fatal("Tried to open more than limit of %d open files in par_oper_onedir",
                          max_open_files);
                }
                my_fd = open(entry->d_name, O_RDONLY);
		if (my_fd < 0) {
		    fatal("Unable to open file [%s/%s] for reading, err=%d",workdir,entry->d_name,errno);
		}
                open_files[count] = my_fd;
                my_fd = -1;
                break;

            case CLOSE_OP:
                my_fd = open_files[count];
                if (close(my_fd)) {
                    fatal("Unable to close file [%s/%s] errno = %d",workdir,entry->d_name,errno);
                }
                my_fd = -1;
		break;

	    case UTIME_OP:
		if (utime(entry->d_name,NULL)) {
		    fatal("Unable to utime file [%s/%s]",workdir,entry->d_name);
		}
		break;

	    case DELETE_OP:
		/* advance to next regular file */
		do {
		    if (lstat(entry->d_name,&st)) {
			fatal("Unable to stat file [%s/%s]",workdir,entry->d_name);
		    }
		    if (S_ISREG(st.st_mode))
			break;
		} while ((entry = readdir(dir)) != NULL);
		if (unlink(entry->d_name)) {
		    fatal("Unable to unlink file [%s/%s]",workdir,entry->d_name);
		}
		break;
		
	    case APPEND_OP:
		/* advance to next regular file */
		do {
		    if (lstat(entry->d_name,&st)) {
			fatal("Unable to stat file [%s/%s]",workdir,entry->d_name);
		    }
		    if (S_ISREG(st.st_mode))
			break;
		} while ((entry = readdir(dir)) != NULL);
		if ( (fd = open(entry->d_name,O_WRONLY|O_APPEND)) == -1) {
		    fatal("Could not open %s in %s for append",
			  entry->d_name,workdir);
		}
		if ( (written = write(fd,buf,nbytes)) != nbytes ) {
		    if (written < 0) {
			fatal("unable to write %ld bytes to %s, fd %d, buf = %0x",
			      nbytes,entry->d_name,fd,(void*)buf);
		    }
		    warning("Short write: %ld bytes of %ld to %s",
			    written,nbytes,entry->d_name);
		}
		if (close(fd)) {
		    fatal("Unable to close %s in %s",entry->d_name,workdir);
		}
		break;
		
	    default:
		fatal("Unknown operation %d",(int)oper);
	    }
	    count++;
    } while ((count < nfilepp) && ((entry = readdir(dir)) != NULL));

    stop_time = getMicrosecondTimeStamp();
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
    elapsed_stop = getMicrosecondTimeStamp();
	     
    if ( closedir(dir) ) {
        fatal("Cannot close dir [%s]",workdir);
    }
    /* get out of Dodge */
    if (chdir(oldpath)) {
	fatal("Can't chmod to [%s]",oldpath);
    }

    /* send results to process 0 */
    dt = 1.0e-6*(double)(stop_time - start_time);
    MPI_SAFE(MPI_Gather(&count,1,MPI_UNSIGNED_LONG,
			count_buf,1,MPI_UNSIGNED_LONG,proc0,MPI_COMM_WORLD));
    MPI_SAFE(MPI_Gather(&dt,1,MPI_DOUBLE,double_buf,1,MPI_DOUBLE,
			proc0,MPI_COMM_WORLD));

    /* report results */
    if (my_rank == proc0) {
	double elapsed = 1.0e-6*(elapsed_stop - start_time);
	if (oper == APPEND_OP) {
	    fprintf(ofl,"File APPEND with %ld bytes by %d processes in the same directory\n",
		    nbytes,num_proc);
	} else if(oper == OPEN_OP) {
	    fprintf(ofl,"File Open-Close by %d processes in the same directory\n",num_proc);
	} else {
	    fprintf(ofl,"File %s by %d processes in the same directory\n",
		    oper_name,num_proc);
	}
	print_results(ofl,count_buf,double_buf,elapsed);
	log_msg("Leaving par_oper_onedir for %s operation with %d processes",
		oper_name,num_proc);
    }

    if (oper == APPEND_OP) {
	free(baseptr);
    } else if (oper == CLOSE_OP) {
        free(open_files);
        open_files = NULL;
    }

    /* wait for all MPI processes at end */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));
}

/* ----------------------------------------------------------------------------
 * Have process proc_id create filecount files in workdir.  We record how long
 * it takes for each rpt_freq files to be created.
 * This gives us an indication the rate at which files can be created
 * as the size of the directory grows.  Some file systems (Ext2&3, for example)
 * create files very quickly, until the directory grows large, then the
 * file creation rate drops off sharply.
 * File systems that store the directory entries in a bananced tree
 * structure have log(N) lookup and file creation times for better
 * overall performance.
 * ----------------------------------------------------------------------------
 */
void time_file_creation(char* workdir, int proc_id, int filecount, int rpt_freq)
{
    struct stat st;
    int num_files = 0;
    int n_create;
    double *dt = NULL;
    uint64_t *count = NULL;
    int num_timings;
    int rptcnt = 0;
    int i;
    double dt_tot = 0.0;
    uint64_t count_tot = 0;
    char oldpath[MAXPATHLEN];
    MPI_Status status;
    int use_node_id = 0;   /* dont have to create globally unique file names */

    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    if (my_rank == proc0) {
	log_msg("Entering time_file_creation with proc_id = %d",proc_id);
    }
    if ((my_rank != proc0) && (my_rank != proc_id)) {
	goto out;
    }


    num_timings = filecount/rpt_freq + 3;
    dt = (double*)malloc(num_timings * sizeof(double));
    memset(dt,0,num_timings * sizeof(uint64_t));
    count = (uint64_t*)malloc(num_timings * sizeof(uint64_t));
    memset(count,0,num_timings * sizeof(uint64_t));

#define COUNT_TAG 666
    if (my_rank == proc_id) {
	/* enter working directory and create the subdirs */
	if (chdir(workdir)) {
	    fatal("Can't chmod to [%s]",workdir);
	}

	while (num_files < filecount) {
	    n_create = MIN(rpt_freq,(filecount-num_files));
	    count[rptcnt] = n_create;
	    dt[rptcnt] = 1.0e-6*(double)create_n_files(n_create,use_node_id,NULL);
	    num_files += n_create;
	    rptcnt++;
	}

	if (chdir(oldpath)) {
	    fatal("Can't chmod to [%s]",oldpath);
	}
	if (proc_id != proc0) {
	    /* send the data to master proc for printing */
	    char *a = (char*)count;
	    MPI_SAFE(MPI_Send(&rptcnt,1,MPI_INT,
			      proc0,COUNT_TAG, MPI_COMM_WORLD));
	    MPI_SAFE(MPI_Send(a,rptcnt*sizeof(uint64_t),MPI_BYTE,
			      proc0,COUNT_TAG, MPI_COMM_WORLD));
	    MPI_SAFE(MPI_Send(dt,rptcnt,MPI_DOUBLE,
			      proc0,COUNT_TAG, MPI_COMM_WORLD));
	}
    } 
    if ((my_rank == proc0) && (proc_id != proc0)) {
	/* receive data */
	char *a = (char*)count;
	MPI_SAFE(MPI_Recv(&rptcnt,1,MPI_INT,proc_id,COUNT_TAG,MPI_COMM_WORLD,&status));
	MPI_SAFE(MPI_Recv(a,rptcnt*sizeof(uint64_t),MPI_BYTE,proc_id,
			  COUNT_TAG,MPI_COMM_WORLD,&status));
	MPI_SAFE(MPI_Recv(dt,rptcnt,MPI_DOUBLE,proc_id,
			  COUNT_TAG,MPI_COMM_WORLD,&status));
    }
    
    if (my_rank == proc0) {
	fprintf(ofl,"File Creation Rates from Process %d\n",proc_id);
	fprintf(ofl,"totfile    tot time   create rate   interval   intv time     intv rate\n");
	fprintf(ofl,"=======  ==========  ============   ========  ==========  ============\n");
        fflush(stdout);
	for (i = 0; i < rptcnt; i++) {
	    double   dt_intv = dt[i];
	    uint64_t cnt_intv = count[i];
	    double   rate, rate_intv;

	    count_tot += cnt_intv;
	    dt_tot += dt_intv;

	    rate = count_tot/dt_tot;
	    rate_intv = cnt_intv/dt_intv;

	    fprintf(ofl,"%7Ld  %10.4f  %12.2f   %8Ld  %10.4f  %12.2f\n",
		    count_tot,dt_tot,rate,
		    cnt_intv,dt_intv,rate_intv);
            fflush(stdout);
	}
	fprintf(ofl,"\n");
	fflush(ofl);
	log_msg("Leaving time_file_creation with proc_id = %d",proc_id);
    }

    free(dt);
    free(count);

    out:

    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

}
	
/* --------------------------------------------------------------------
 * Compute and return the average (mean) value and/or the
 * standard deviation of a sequence of data values.
 * --------------------------------------------------------------------
 */
void compute_stats(int n, double *data, double *mean, double *sigma)
{
    double avg = 0.0;
    double tmp = 0.0;
    int i;

    if (n <= 0) {
	if (mean != NULL)
	    *mean = 0.0;
	if (sigma != NULL)
	    *sigma = 0.0;
	return;
    }

    for (i = 0; i < n; i++) {
	avg += data[i];
    }
    avg /= (double)n;

    if (mean != NULL)
	*mean = avg;

    if (sigma == NULL) {
	return;
    }

    for (i = 0; i < n; i++) {
	tmp += (data[i] - avg)*(data[i] - avg);
    }
    tmp /= (double)n;
    *sigma = sqrt(tmp);
}

/* --------------------------------------------------------------------
 * Print per-process and aggregate results of test to given file.
 * --------------------------------------------------------------------
 */
void print_results(FILE *out, unsigned long *files, double *dt, double elapsed_dt)
{
    int rank;
    double max_dt = 0.0;
    double tot_rate, elapsed_rate;
    double avg_time, sigma_time;
    double avg_rate, sigma_rate;
    unsigned long  avg_files;
    unsigned long  tot_files = 0;

    for (rank = 0; rank < num_proc; rank++) {
	max_dt = MAX(max_dt,dt[rank]);
    }

    fprintf(out,"%-13s %12.12s %10s %10s %10s\n","Process","Hostname","Files","Time","Rate");
    fprintf(out,"------------- ------------ ---------- ---------- ----------\n");
    fflush(out);
    for (rank = 0; rank < num_proc; rank++) {
	rate[rank] = ((double)files[rank])/dt[rank];
	tot_files += files[rank];
	fprintf(out,"%06d/%06d %12.12s %10ld %10.3f %10.3f\n",
		rank,num_proc,all_hostname.array[rank],files[rank],dt[rank],rate[rank]);
    }
    fflush(out);
    fprintf(out,"------------- ------------ ---------- ---------- ----------\n");
    tot_rate = ((double)tot_files)/max_dt;
    fprintf(out,"Total                      %10ld %10.3f %10.3f\n",tot_files,max_dt,tot_rate);
    fflush(out);
    elapsed_rate = ((double)tot_files)/elapsed_dt;
    fprintf(out,"Elapsed                    %10ld %10.3f %10.3f\n",tot_files,elapsed_dt,elapsed_rate);
    fflush(out);
    if (num_proc > 1) {
	fprintf(out,"------------- ------------ ---------- ---------- ----------\n");
	compute_stats(num_proc,&dt[0],&avg_time,&sigma_time);
	compute_stats(num_proc,&rate[0],&avg_rate,&sigma_rate);
	avg_files = (unsigned long)(tot_files/num_proc);
	fprintf(out,"Average                    %10ld %10.3f %10.3f\n",
                avg_files,avg_time,avg_rate);
	fprintf(out,"Std Dev                               %10.3f %10.3f (%7.2f%%) (%7.2f%%)\n",
		sigma_time,sigma_rate,100.0*(sigma_time/avg_time),
		100.0*(sigma_rate/avg_rate));
    }
    fprintf(out,"\n");
    fflush(out);
}

    
/*
 * ----------------------------------------------------------------------------------
 * Generate a set of communicators that contain a partitioning of all processes
 * such that each communicator contains exactly one process on each node.
 * ----------------------------------------------------------------------------------
 */
void create_shift_comm(void)
{
    int    i;
    int    rank;
    int    my_row;

    /* MLW: use MPI_Gather? */

    /* Proc 0 collects the hostnames from all others */
    if (my_rank == proc0) {
        new_str_array(num_proc,MAX_HOSTNAME,&all_hostname);
    }
    MPI_SAFE(MPI_Gather(hostname, MAX_HOSTNAME, MPI_CHAR,
                        all_hostname.buffer, MAX_HOSTNAME, MPI_CHAR,
                        proc0, MPI_COMM_WORLD));

    if (my_rank == proc0) {
	for (rank = 0; rank < num_proc; rank++) {
            fprintf(ofl,"Rank %6d process on node %s\n",rank,all_hostname.array[rank]);
        }
        fprintf(ofl,"\n");
    }

    /* For convenience, map hostnames to unique integer IDs, since working
     * with char strings is a pain
     */
    host_ids = (int*)malloc(num_proc * sizeof(int));
    if (host_ids == NULL) {
        fatal("Cant alloc host_ids array");
    }

    /* process 0 does the mapping and distributes it */
    if (my_rank == proc0) {
        char** hostmap = (char**)malloc(num_proc * sizeof(char*));
        
        if (hostmap == NULL) {
            fatal("Cant alloc hostmap");
        }
        for (i = 0; i < num_proc; i++) {
            hostmap[i] = NULL;
        }

        for (rank = 0; rank < num_proc; rank++) {
            /* search map for this hostname, if found use the index
               as the hostid, if not put it in the map.
            */
            for (i = 0; i < num_proc; i++) {
                if (hostmap[i] == NULL) {
                    /* not found in existing list, add it */
                    hostmap[i] = all_hostname.array[rank];
                    host_ids[rank] = i;
                    num_hosts++;
                    break;
                } else if (strncmp(hostmap[i],all_hostname.array[rank],MAX_HOSTNAME) == 0) {
                    /* found a match */
                    host_ids[rank] = i;
                    break;
                }
            }
        }

        /* print out the mapping if requsted */ 
        fprintf(ofl,"There are %d unique hosts detected\n",num_hosts);
        if (verbose) {
            for (i = 0; i < num_hosts; i++) {
                fprintf(ofl,"%6d => %s\n",i,hostmap[i]);
            }
            fprintf(ofl,"\n");
        }
        free(hostmap);
    }

    /* Send the number of distinct hosts to all processes */
    MPI_SAFE(MPI_Bcast(&num_hosts, 1, MPI_INT, proc0, MPI_COMM_WORLD));

    /* ok, now broadcast the host_ids map to all processes */
    MPI_SAFE(MPI_Bcast(host_ids, num_proc, MPI_INT, proc0, MPI_COMM_WORLD));

    /* now each process must discover its host_id */
    my_hostid = host_ids[my_rank];

    /* We effectively want to create a 2D array with all entries in a column
       are the global ranks of processes on the same host.  The rows will then
       form a disjoint union of all processes, where each process in a row
       is on a distinct host.
       We want the communicators to be the rows.  To do this, we color each process
       by the number of other processes are in the list before it on the same host.
    */
    my_row = -1;
    for (rank = 0; rank < num_proc; rank++) {
        if (host_ids[rank] == my_hostid) {
            my_row++;
        }
        if (rank == my_rank) {
            break;
        }
    }
    if (my_row == -1) {
        fatal("Could not compute row number on proc %d",my_rank);
    }

    /* ok, compute the communicator */
    MPI_SAFE(MPI_Comm_split(MPI_COMM_WORLD, my_row, my_hostid, &row_comm));

    /* get the size of this communicator and my rank in it */
    MPI_SAFE(MPI_Comm_size(row_comm, &row_comm_size));
    MPI_SAFE(MPI_Comm_rank(row_comm, &my_row_rank));

    if (test_row_communicator) {
        row_comm_test();
    }

}

/*
 * ----------------------------------------------------------------------------------
 * Use the row_comm communicator to rotate the input directory
 * name around the ring of processors in the row communicator
 * ----------------------------------------------------------------------------------
 */
void rotate_strings(char *string, int str_len)
{
    int send_to   = (my_row_rank + 1) % row_comm_size;
    int recv_from = my_row_rank - 1;
    int tag = 999;
    MPI_Status   status;

    if (recv_from < 0) {
        recv_from = row_comm_size - 1;
    }

    MPI_SAFE(MPI_Sendrecv_replace(string, str_len, MPI_CHAR, send_to, tag,
                                  recv_from, tag, row_comm, &status));
}

/*
 * ----------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------
 */
void print_string_all(char *str, string_arr_t *all_strings)
{
    int rank;
    int str_len = all_strings->str_len;

    MPI_SAFE(MPI_Gather(str, str_len, MPI_CHAR,
                        all_strings->buffer, str_len, MPI_CHAR,
                        proc0, MPI_COMM_WORLD));

    if (my_rank == proc0) {
        printf("Mapping of rank to string values\n");
        for (rank = 0; rank < num_proc; rank++) {
            printf("%6d  => %s\n",rank,all_strings->array[rank]);
        }
        printf("\n");
    }
    
}

/*
 * ----------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------
 */
void row_comm_test(void)
{
    /* each process creates a string */
    int  str_len = 128;
    char mystring[128];
    int  i;
    string_arr_t all_strings;

    if (my_rank == proc0) {
        new_str_array(num_proc,str_len,&all_strings);
    }

    sprintf(mystring,"proc_%05d",my_rank);
    print_string_all(mystring,&all_strings);

    for (i = 0; i < 4; i++) {
        rotate_strings(mystring,str_len);
        print_string_all(mystring,&all_strings);
    }

    if (my_rank == proc0) {
        free_str_array(&all_strings);
    }
}

/*
 * ----------------------------------------------------------------------------------
 * Parse the command line options.
 *
 * Note thta MPI does not guarantee that all processes have local access to
 * the command line options.  To be technically correct, proc0 should parse
 * the values and broadcast them to all other processes.
 *
 * USAGE:
 *          -d          => debug mode
 *          -v          => verbose mode
 *          -k          => dont cleanup files when finished
 *          -T nfile    => Timed file create test by one process
 *          -f count    => timed file creation report frequency (num files)
 *          -c nfilepp  => Each proc creates nfilepp files in seperate dirs
 *          -C nfilepp  => Each proc creates nfilepp in SAME dir
 *          -S          => Run file stats test
 *          -U          => Run file utime test
 *          -A bytes    => Run file append test with this number of bytes
 *          -w dir      => Specify path to top level working directory
 *          -p dict     => Specify path to dictionary file for name generation
 *          -o path     => Specify output file name (default = stdout)
 *          -n rank     => Run the filename generation test (debug option)
 *          -t          => Test row communicator (debug option)
 *          -r          => Rotate per-process dirs between tests
 *          -P          => Preallocate filenames outside timing loop
 *
 * ----------------------------------------------------------------------------------
 */
void parse_args(int argc, char *argv[] )
{
    int c;
    int once = 0;

#if 0
    int i;
    printf("parse_args: the arguments are:\n");
    for (i = 0; i < argc; i++) {
	printf("%s ",argv[i]);
    }
    printf("\n\n");
    fflush(stdout);
#endif

    opterr = 0;
    while (1)
    {
	c = getopt( argc, argv, "dYvkA:c:C:T:l:SUDOn:f:w:p:o:trP");

	if (c == -1)
	    break;
	
	switch (c)
	{

	case 'd':
	    debug = 1;
	    break;

	case 'Y':
	    delay_create = 1;
	    break;

	case 'v':
	    verbose = 1;
	    break;

	case 'k':
            /* keep dirs and remaining files */
	    do_cleanup = 0;
	    break;

	case 'A':
	    run_test[APPEND_OP] = 1;
	    file_append_size = scale_int(optarg);
	    break;

	case 'c':
            multidir_nfilepp = (unsigned long)atol(optarg);
	    break;

	case 'C':
            onedir_nfilepp = (unsigned long)atol(optarg);
	    break;

	case 'T':
            timed_create_nfile = (unsigned long)atol(optarg);
	    break;

	case 'S':
	    run_test[STAT_OP] = 1;
	    break;

	case 'U':
	    run_test[UTIME_OP] = 1;
	    break;

	case 'O':
	    run_test[OPEN_OP] = 1;
	    break;

	case 'D':
	    run_test[DELETE_OP] = 1;
	    break;

	case 'l':
	    oper_loop_count = atoi(optarg);
	    break;

	case 'n':
	    test_namegen_count = atoi(optarg);
            testnamegen = 1;
	    break;

	case 'f':
	    report_freq = atoi(optarg);
	    break;

	case 'w':
	    workdir_root = optarg;
	    break;

	case 'p':
	    dictionary_path = optarg;
	    break;

	case 'o':
	    testname = optarg;
	    break;

	case 't':
	    test_row_communicator = 1;
	    break;

	case 'r':
	    rotate_dirs = 1;
	    break;

	case 'P':
	    prealloc_filenames = 0;
	    break;

	case '?':
	default:	    
	    printf("Invalid Arg: %c\n",c);
	    exit(1);
	    break;
	}
    }
}
