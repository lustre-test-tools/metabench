/*
Metabench Copyright (c) 2004, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov
referring to "Metabench (LBNL Ref CR-2058)"

NOTICE.  This software was developed under funding from the U.S. Department
of Energy.  As such, the U.S. Government has been granted for itself and
others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide
license in the Software to reproduce, prepare derivative works, and perform
publicly and display publicly.  Beginning five (5) years after the date
permission to assert copyright is obtained from the U.S. Department of Energy,
and subject to any subsequent five (5) year renewals, the U.S. Government is
granted for itself and others acting on its behalf a paid-up, nonexclusive,
irrevocable, worldwide license in the Software to reproduce, prepare
derivative works, distribute copies to the public, perform publicly and
display publicly, and to permit others to do so.
*/

/* ==========================================================================
 * The routines in this file implement the generation of file names for the
 * Metabench file creation tests.  The requirement is that each MPI process
 * must generate unique file names within a test.  In addition, for tests in
 * which all processes create files within one directory, all filenames across
 * all processes must be unique.
 * In addition, we would like the names to have variable lengths and not all
 * look like file000001, file0000002, etc.
 * To do this, we read in a dictionary of english words and use a random
 * number generator to select a word from the dictionary.  In addition, we
 * record the number of times the word has been selected and add a unique
 * suffix to make the filename unique within that MPI process.
 * Finally, to make the file names unique across all processes, a second
 * filename suffix is added which is unique to each process.
 * The filenames then have the format:
 *      <dictionary_word>[_process_suffix][.<count_suffix>]
 * both process_suffix and count_suffix are strings that represent numbers
 * in a base 62 number systems in which the characters:
 *           0-9    represent the digits  0-9  base 62
 *           a-z    represent the digits 10-35 base 62
 *           A-Z    represent the digits 36-61 base 62
 * process_suffix is the MPI rank of the process in this base
 * count_suffix   is the count of the number of times <dictionary_word> was
 * selected by this process.  The suffic is ommitted the first time the word
 * is selected.  So, if the word television is selected 3 times, the generated
 * names will be:
 *                         television
 *                         television.1
 *                         television.2
 * The dictionary has about 430,000 entries, but we only select words that have
 * less than 16 characters (still over 400,000 words).
 *
 * The number of digits used in the count_suffix will depend on how many files
 * need to be created.  We use up to 5 digits to the totol number of per-
 * process unique files is about 400000 * 62^5 > 350 trillion files.
 *
 * Note that for parallel jobs, the dictionary is read from disk by process 0
 * and broadcast to all other processes.
 * At most, the dictionary will consume about 430000*(2+sizeof(int)) bytes
 * of memory, which is at most 4.3 MB per node.  Peanuts!
 *
 * The dictionary counters should be reset between each test since the files
 * for each test are written to different directories and therefore the names
 * from the old test can be re-used.
 * ==========================================================================
 */

#include "metabench.h"

/* ==========================================================================
 * Dictionary static values
 * ==========================================================================
 */

extern int verbose;
extern int debug;
static int limit_dictionary = 0;

typedef struct {
    char     word[16];
    int      count;
} dictionary_entry_t;

#define ALPH_LEN 62
static long                 num_dict_entries = 0;
static dictionary_entry_t  *dictionary = NULL;
static int                  suffix_len = 0;
static int                  max_suffix_val = 0;
static char                 alphabet[ALPH_LEN];
static int                  node_id_len = 0;
static char                *my_id_str = NULL;


/* ==========================================================================
 * Count the number of words in the dictionary file that are less than 16
 * characters in length.
 * ==========================================================================
 */
static long count_dictionary(char *dict_path)
{
    long  count = 0;
    char  buf[256];
    char  *p;
    FILE *df = fopen(dict_path,"r");

    if (df == NULL) {
        fatal("Could not open dictionary file [%s]",dict_path);
    }
    while ( (p = fgets(buf,256,df)) != NULL) {
        if (strlen(p) < 16) {
            count++;
        }
    }
    fclose(df);

    if (verbose) {
        fprintf(stderr,"Found %ld dictionary words of length < 16 char in %s\n",count,dict_path);
    }
    
    return count;
}
    
/* ==========================================================================
 * Convert the integer nvalue into a string of digits that represent the 
 * value in base 62 with alphabet [0-9a-zA-Z].
 * The string will have a length suffix_len+2 characters in which the
 * leftmost character is ".", the value of nvalue is encoded into the
 * next suffic_len chars, and is terminated by a null character.
 *
 * It is assumed the "str" argument is a character buffer with at least
 * suffix_len+2 characters.
 * ==========================================================================
 */
static void gen_suffix(char *str, int nvalue)
{
    int  pos;
    int  value = nvalue;

    if (nvalue == 0) {
        /* dont add a suffix for the first time word is used */
        str[0] = '\0';
        return;
    }
    str[suffix_len+1] = '\0';
    pos = suffix_len;
    while (pos > 0) {
        str[pos--] = alphabet[value % ALPH_LEN];
        value = (long)(value / ALPH_LEN);
    }
    str[0] = '.';
    if (debug) {
        printf("Val = %d  Suffix = [%s]\n",nvalue,str);
    }
}

/* ==========================================================================
 * Convert the integer nvalue into a character string of length nchar
 * representing the value in base 62 with the alphabet [0-9a-zA-Z].
 * The string is inserted into "str" with the rightmost character
 * written at position "pos".
 * ==========================================================================
 */
static void insert_from_alphabet(char *str, int pos, int nchar, int nvalue)
{
    int written = 0;

    for (written = 0; written < nchar; written++) {
        if (pos < 0) {
            fatal("ifa: pos = %d, nchar = %d",pos,nchar);
        }
        str[pos--] = alphabet[nvalue % ALPH_LEN];
        nvalue = (long)(nvalue / ALPH_LEN);
    }
}

/* ==========================================================================
 * Init the dictionary.
 * This routine performs several tasks to init the dictionary and filename
 * generation data structures:
 *
 *  (1) Inits the global alphabet string
 *  (2) Determines the number of characters needed to encode the rank of
 *      the largest MPI process rank as a base 62 number.
 *  (3) Construct the ID string for the rank of this MPI process.
 *  (4) (proc 0) Count the number of words in the dictionary.
 *  (5) (proc 0) Based on the number of files to be generated, determine how 
 *      many words to read in from the dictionary.  In addition, determine the 
 *      length of the count_suffix so that the number of possible unique words 
 *      is at least twice the number of unique filenames to be generated.
 *  (6) (proc 0) read the dictionary.
 *  (7) proc 0 broadcasts the suffix length, size of the dictionary and the
 *      dictionary itself to all other processes.
 * ==========================================================================
 */
void init_dictionary(char *dict_path, long num_files)
{
    FILE *df = NULL;
    char  buf[256];
    char  word[256];
    char  *p;
    int   i, k;
    int   crange = 0;
    int   pos;
    int   skip = 0;
    long  count = 0;
    long  dict_words, max_entry;

    /* (1) set up the alphabet for the suffix chars */
    for (i = 0; i < 10; i++) {
        alphabet[crange++] = '0' + i;
    }
    for (i = 0; i < 26; i++) {
        alphabet[crange++] = 'a' + i;
    }
    for (i = 0; i < 26; i++) {
        alphabet[crange++] = 'A' + i;
    }
    assert(crange == ALPH_LEN);

    /* (2)  node_count is the number of MPI tasks.  Determine the minimum string
     * length, taken from the alphabet of ALPH_LEN chars, needed to uniquely
     * identify each node
     */
    node_id_len = 0;
    {
        int np = num_proc;
        while (np > 0) {
            node_id_len++;
            np = (int)(np / ALPH_LEN);
        }
    }
    my_id_str = (char*)malloc((node_id_len + 2)*sizeof(char));
    if (my_id_str == NULL) {
        fatal("Out of memory allocating %d chars for id string",node_id_len+1);
    }

    /* (3) now construct the id string */
    pos = node_id_len;
    my_id_str[node_id_len+1] = '\0';
    insert_from_alphabet(my_id_str,node_id_len,node_id_len,my_rank);
    my_id_str[0] = '_';

    if (verbose) {
        fprintf(stderr,"My Rank = %4d, ID Len = %3d, My ID String = [%s]\n",
                my_rank, node_id_len, my_id_str);
    }

    if (my_rank == proc0) {

        /* (4) only process 0 reads the dictionary file.  Here we count entries */
        dict_words = count_dictionary(dict_path);

        /* (5) determine how much of this we should limit the number of
         * words we read from the dictionary.
         * 400000 words is just not that much, so this option is used
         * mostly for testing the name generation algorithm.
         */
        if (limit_dictionary) {
            if (num_files < (0.125*dict_words) ) {
                max_entry = (int)(0.25 * dict_words);
            } else if (num_files < (0.25 * dict_words) ) {
                max_entry = (int)(0.5 * dict_words);
            } else {
                max_entry = dict_words;
            }
        } else {
            max_entry = dict_words;
        }
        assert(max_entry > 0);
        
        if (max_entry >= dict_words) {
            skip = 0;
        } else {
            skip = (int)(dict_words/max_entry);
        }        

        max_suffix_val = 1;
        suffix_len = 0;
        for (k = 1; k <= 5; k++) {
            unsigned long max_files;
            max_suffix_val *= ALPH_LEN;

            max_files = max_entry * max_suffix_val;
            if (num_files < ( max_files / 2)) {
                suffix_len = k;
                break;
            }
        }
        if (suffix_len == 0) {
            fatal("Too many files requested");
        }


        /* (6) on proc0, we allocate enough space for max_entry entries.  We may
         * not actually read that many
         */
        dictionary = (dictionary_entry_t*)malloc(max_entry*sizeof(dictionary_entry_t));
        if (dictionary == NULL) {
            fatal("Out of memory allocating %d dictionary entries",max_entry);
        }

        /* Dictionary is in sorted order.  Decide on how many words we want
         * to save and how many to skip between reads
         */
        if (verbose) {
            fprintf(stderr,"Allocated %ld directory entries of %ld total entries\n",
                    max_entry,dict_words);
            fprintf(stderr,"Skipping %d entries between selected words from dictionary\n",skip);
        }
    

        /* now actually read it */
        df = fopen(dict_path,"r");
        if (df == NULL) {
            fatal("Could not open dictionary file [%s]",dict_path);
        }

        count = 0;
        for (;;) {
            if (count >= max_entry) break;

            /* read the next set of words and keep the first with < 16 chars */
            int found = 0;
            int iter = 0;
            while ( (! found) || (iter < skip) ) {
                int len;
                p = fgets(buf,256,df);
                if (p == NULL) break;
                len = strlen(p);
                if ( (! found) && (len < 16) ) {
                    found = 1;
                    /* remove newline char */
                    p[len-1] = '\0';
                    strncpy(word,p,16);
                }
                iter++;
            }

            if (!found) break;

            if (count < max_entry) {
                /* ok, save this one */
                strncpy(dictionary[count].word,word,16);
                dictionary[count].count = 0;
                count++;
            }
        }

        if (verbose) {
            fprintf(stderr,"Read %ld dictionary words\n",count);
        }

        num_dict_entries = count;
    }

    /* (7) now broadcast the number of words in the dictionary to all */
    MPI_SAFE(MPI_Bcast(&suffix_len, 1, MPI_INT, proc0, MPI_COMM_WORLD));

    /* everyone computes max_suffix_value */
    max_suffix_val = 1;
    for (k = 1; k <= suffix_len; k++) {
        max_suffix_val *= ALPH_LEN;
    }
    if (verbose) {
        fprintf(stderr,"Suffix length is %d, max value = %d\n",suffix_len,max_suffix_val);
    }

    /* now broadcast the number of words in the dictionary to all */
    MPI_SAFE(MPI_Bcast(&dict_words, 1, MPI_LONG, proc0, MPI_COMM_WORLD));

    /* now broadcast the number of entries proc0 has read */
    MPI_SAFE(MPI_Bcast(&num_dict_entries, 1, MPI_LONG, proc0, MPI_COMM_WORLD));

    if (my_rank != proc0) {
        /* now that all procs know the size, alloc dictionary */
        dictionary = (dictionary_entry_t*)malloc(num_dict_entries*sizeof(dictionary_entry_t));
        if (dictionary == NULL) {
            fatal("Out of memory allocating %d dictionary entries",num_dict_entries);
        }
    }

    /* lets make sure everyone has allocated the space before we blast it */
    MPI_SAFE(MPI_Barrier(MPI_COMM_WORLD));

    /* finally, distribute the dictionary */
    {
        long dictionary_sz = num_dict_entries * sizeof(dictionary_entry_t);
        MPI_SAFE(MPI_Bcast(dictionary, dictionary_sz, MPI_BYTE, proc0, MPI_COMM_WORLD));
    }

}

/* ==========================================================================
 * reset the dictionary use counters
 * ==========================================================================
 */
void reset_dictionary()
{
    int i;
    for (i = 0; i < num_dict_entries; i++) {
        dictionary[i].count = 0;
    }
}

/* ==========================================================================
 * This is a diagnostic routine that prints the number of dictionary entries
 * used to date, and the min and max number of times any word was used.
 * ==========================================================================
 */
void print_dictionary_stats()
{
    int i;
    int max = 0;
    int min = 9999999;
    int num_zero = 0;
    int maxix = -1;
    int minix = -1;
    int numdup = 0;
    char *maxstr;
    char *minstr;
    for (i = 0; i < num_dict_entries; i++) {
        int cnt = dictionary[i].count;
        if (cnt > 1) {
            numdup++;
        }
        if (cnt == 0) {
            num_zero++;
        } else {
            if (cnt >= max) {
                max = cnt;
                maxstr = dictionary[i].word;
            }
            if (cnt < min) {
                min = cnt;
                minstr = dictionary[i].word;
            }
        }
    }

    fprintf(stderr,"Rank %4d: %d of %ld = %6.2f precent entries never used\n",
            my_rank,num_zero,num_dict_entries,(100.0*num_zero)/num_dict_entries);
    fprintf(stderr,"Rank %4d: %d = %6.2f percent of entries used multiple times\n",
            my_rank,numdup,(100.0*numdup)/num_dict_entries);
    fprintf(stderr,"Rank %4d: Min used word = [%s] used %d times\n",my_rank,minstr,min);
    fprintf(stderr,"Rank %4d: Max used word = [%s] used %d times\n",my_rank,maxstr,max);
}

/* ==========================================================================
 * Use dictionary to generate filenames that are unique within an MPI task
 * if use_nod_id is true, then gen unique names across the entire job by
 * inserting a process_suffix before the count_suffix.
 * It is assumed that "string" points to a character buffer of sufficient
 * size to handle the generated name: 18 + suffix_len [+ process_suffix_len].
 * ==========================================================================
 */
void gen_filename(char *string, int use_node_id) 
{
    unsigned long ix = random();
    unsigned long first_ix;
    int suf_val;
    char suffix_str[64];
    *string = '\0';

    /* clip the random index to be within the range of the dictionary */
    ix = (ix % num_dict_entries);
    first_ix = ix;

    int found = 0;
    while (! found) {
        if (dictionary[ix].count >= max_suffix_val) {
            /* this entry used the max number of times, try next */
            ix = (ix + 1) % num_dict_entries;
            if (ix == first_ix) {
                /* checked each entry and all used max number of times */
                break;
            }
        } else {
            found = 1;
        }
    }

    if (!found) {
        fatal("Ran out of dictionary entries");
    }

    suf_val = dictionary[ix].count;
    dictionary[ix].count++;

    /* insert the dictionary word */
    strcat(string,dictionary[ix].word);

    if (use_node_id) {
        /* insert the node id string */
        strcat(string,my_id_str);
    }

    /* generate and add the suffix */
    gen_suffix(suffix_str,suf_val);
    strcat(string,suffix_str);
}

/* ==========================================================================
 * Testing routine to generate filenames and print stats to stderr
 * ==========================================================================
 */
void test_namegen (long file_count)
{
    long    i;
    char    str[MAXPATHLEN];
    time_t  start_time, stop_time;
    int     use_unique = 0;

    verbose = 1;
    time(&start_time);
    for (i = 0; i < file_count; i++) {
        gen_filename(str, use_unique);
        /*        printf("%s\n",str); */
    }
    time(&stop_time);
    fprintf(stderr,"Rank %4d: Generating %ld names took %d seconds\n",
            my_rank, file_count, (int)(stop_time - start_time) );
    print_dictionary_stats();
    
}
