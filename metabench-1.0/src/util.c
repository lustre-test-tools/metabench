/*
Metabench Copyright (c) 2004, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov
referring to "Metabench (LBNL Ref CR-2058)"

NOTICE.  This software was developed under funding from the U.S. Department
of Energy.  As such, the U.S. Government has been granted for itself and
others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide
license in the Software to reproduce, prepare derivative works, and perform
publicly and display publicly.  Beginning five (5) years after the date
permission to assert copyright is obtained from the U.S. Department of Energy,
and subject to any subsequent five (5) year renewals, the U.S. Government is
granted for itself and others acting on its behalf a paid-up, nonexclusive,
irrevocable, worldwide license in the Software to reproduce, prepare
derivative works, distribute copies to the public, perform publicly and
display publicly, and to permit others to do so.
*/

#include "metabench.h"
#include <stdarg.h>

/*
 * create the named subdir if it does not already exist.
 * Return failure if dirname exists and is not a directory
 * or if we cannot create it.
 */
int create_subdir(char* dirname)
{
    struct stat st;

    if( lstat(dirname,&st) ) {
	if (errno == ENOENT) {
	    /* doesn't exist, make it */
	    if (mkdir(dirname,dir_mode)) {
		return 0;
	    }
	}
    }
    /* it either existed before, or does now, stat again */
    if( lstat(dirname,&st) ) {
	fatal("Cant stat [%s]",dirname);
    }
    if (S_ISDIR(st.st_mode)) {
	/* its a directory, we are ok */
	return 1;
    }
    /* its not a directory, but may be a symlink that points to one */
    if (! S_ISLNK(st.st_mode)) {
	/* not a dir or a symlink, fail */
	return 0;
    }
    /* its a symlink, does it point to a dir? */
    if( stat(dirname,&st) ) {
	/* failed stat-ing the target */
	return 0;
    }
    if (S_ISDIR(st.st_mode)) {
	/* target is a dir, ok */
	return 1;
    }
    /* target is not a directory, fail */
    /* Note: could be another sym link, but lets not allow that! */
    return 0;
}


/* -----------------------------------------------------------
 * Create a path to a directory as specified by the argument.
 *
 * This is done my walking the path and if a component
 * subdirectory is missing, create it.  Fail if one
 * of the components exists and is not a directory or
 * link to a directory.  Also fail of we do not have
 * permissions to create the directory.
 * -----------------------------------------------------------
 */
void create_path(char *path)
{
     struct stat st;
     char *p = NULL;
     char oldpath[MAXPATHLEN];
     char newpath[MAXPATHLEN];
     char *comp = NULL;
     const char *delim = "/";

     strncpy(&newpath[0],path,MAXPATHLEN);
     p = newpath;
     
     /* save the old path */
     getcwd(oldpath,MAXPATHLEN);

     /* be paranoid and strip leading white space */
     while (isblank(*p)) p++;
	    
     /* first, see if this is a full path */
     if (*p == '/') {
	 p++;
	 if (chdir("/")) {
	     fatal("Can't chmod to /");
	 }
     }

     /* now walk the components */
     comp = strtok(p,delim);
     p = NULL;

     while (comp != NULL) {
	 if (! create_subdir(comp) ) {
	     fatal("cannot create component %s in %s",comp,path);
	 }
	 if (chdir(comp)) {
	     fatal("Can't chmod to %s in %s",comp,path);
	 }
	 comp = strtok(NULL,delim);
     }

     /* cd back to where we started */
     if (chdir(oldpath)) {
	 fatal("cannot cd to %s",oldpath);
     }
}

/* -----------------------------------------------------------
 * Function to free an array of string
 * -----------------------------------------------------------
 */
void free_str_array(string_arr_t *array)
{
    free(array->buffer);
    free(array->array);
}

/* -----------------------------------------------------------
 * Function to allocate an array of strings
 * -----------------------------------------------------------
 */
void new_str_array(unsigned long count,
                   int           str_len,
                   string_arr_t *str_array)
{
    unsigned long i;
    char *p;
    if (str_array == NULL) {
        fatal("new_str_array with null str_array");
    }
    str_array->count = count;
    str_array->str_len = str_len;
    p = str_array->buffer = (char*)malloc(count * str_len * sizeof(char));
    if (str_array->buffer == NULL) {
        fatal("new_str_array buffer malloc failed with str_len = %d, count=%lu",
              str_len,count);
    }
    str_array->array = (char**)malloc(count * sizeof(char*));
    if (str_array->array == NULL) {
        fatal("new_str_array array malloc failed count=%lu",count);
    }
    for (i = 0; i < count; i++) {
        str_array->array[i] = p;
        p += str_len;
    }
}

/* -----------------------------------------------------------
 * Allocate an array of string of the proper length,
 * then create filecount random but unique filename
 * The caller is responsible for freeing the files array
 * -----------------------------------------------------------
 */
void gen_n_filenames(unsigned long filecount,
                     int           str_len,
                     int           use_node_id,
                     string_arr_t *files)
{
    int i;

    new_str_array(filecount,str_len,files);

    for (i = 0; i < filecount; i++) {
	gen_filename(files->array[i],use_node_id);
    }
}

/* -----------------------------------------------------------
 * Function to create N files in current directory
 * and return elapsed time in microseconds
 * -----------------------------------------------------------
 */
#ifdef HAS_LUSTRE
#include <lustre/lustre_user.h>
#endif
uint64_t create_n_files(unsigned long filecount,
                        int           use_node_id,
                        string_arr_t *files)
{
    uint64_t start_time, stop_time;
    char filebuffer[MAXPATHLEN];
    char* filename = NULL;
    unsigned long i, num_files = 0;
    int fd;
    int fd_flags = O_CREAT | O_WRONLY | O_TRUNC;

#ifdef HAS_LUSTRE    
    if (delay_create) {
        fd_flags |= O_EXCL | O_LOV_DELAY_CREATE;
    }
#endif        

    start_time = getMicrosecondTimeStamp();
    for (i = 0; i < filecount; i++) {
        /* if file names have already been generated, use them */
        if (files != NULL) {
            filename = files->array[i];
        } else {
            gen_filename(filebuffer,use_node_id);
            filename = &filebuffer[0];
        }
        /*	if ((fd=creat(filename,file_mode)) == -1) { */
	if ((fd=open(filename,fd_flags,file_mode)) == -1) {
	    if (errno == EEXIST) {
                fatal("Filename Collision on file number [%d] named [%s]",
                      i,filename);
	    }
	    fatal("Could not create file number [%d] named [%s]",
		  i,filename);
	}
	if (close(fd)) 
	    fatal("Could not close file [%s]",filename);
    }
    stop_time = getMicrosecondTimeStamp();

    return (stop_time - start_time);
}

/* -----------------------------------------------------------
 * Simple variation of above in which we first create the
 * named test directory (if it does not already exist)
 * then cd into it, create the files and cd back to where
 * we started.
 * -----------------------------------------------------------
 */
uint64_t create_subdir_and_files(char* newdir,
                                 int   use_node_id,
                                 unsigned long num_files)
{
    uint64_t usec;
    char oldpath[MAXPATHLEN];

    /* save the current working dir */
    if (getcwd(oldpath,MAXPATHLEN) == NULL) {
	fatal("Cant get cwd");
    }

    if (! create_subdir(newdir)) {
	fatal("cannot create subdir [%s] in [%s]",newdir,oldpath);
    }
    if (chdir(newdir)) {
	fatal("cannot cd to [%s] from [%s]",newdir,oldpath);
    }
    usec = create_n_files(num_files,use_node_id,NULL);
    if (chdir(oldpath)) {
	fatal("cannot cd to [%s]",oldpath);
    }
    return usec;
}

/* This function removes all the files in the given
 * directory.  If recurse == TRUE it will recursively
 * delete all subdirs as well.
 * Note that is does not remove the main directory,
 * given by the "path" argument
 */
void clear_dir(char* path, int recurse)
{
    struct stat st;
    uint64_t start_time, stop_time;
    double dt;
    DIR *dir;
    struct dirent *entry;
    char oldpath[MAXPATHLEN];
    char newpath[MAXPATHLEN];
    static int level = 0;
    static int dirs = 0;
    static int files = 0;
    static int dir_fail = 0;
    static int file_fail = 0;

    /* save the old path */
    getcwd(oldpath,MAXPATHLEN);

    if (level == 0) {
	/* re-init static counters */
	dirs = files = 0;
        dir_fail = file_fail = 0;
    }

    /* first we check that dir exists, then cd to it */
    if( stat(path,&st) ) {
	warning("Cant stat dir [%s]",path);
        dir_fail++;
        return;
    }
    if (!S_ISDIR(st.st_mode)) {
	fatal("[%s] is not a directory",path);
    }
    if (chdir(path)) {
	fatal("Can't chmod to [%s]",path);
    }
    if ( (dir = opendir(".")) == NULL) {
	fatal("Cannot open directory [%s] for walking",path);
    }

    start_time = getMicrosecondTimeStamp();

    while ( (entry = readdir(dir)) != NULL ) {
	if (lstat(entry->d_name,&st)) {
	    warning("Unable to stat file [%s/%s]",path,entry->d_name);
            file_fail++;
            continue;
	}
	if (S_ISDIR(st.st_mode) ) {
	    if (! recurse)
		continue;
	    if (strcmp(entry->d_name,".") == 0)
		continue;
	    if (strcmp(entry->d_name,"..") == 0)
		continue;
	    sprintf(newpath,"%s/%s",path,entry->d_name);
	    level++;
	    clear_dir(entry->d_name,recurse);
	    level--;
	    if (rmdir(entry->d_name)) {
                warning("Cant remove directory [%s]",newpath);
                dir_fail++;
	    } else {
                dirs++;
            }
	} else {
	    if (unlink(entry->d_name)) {
		warning("Unable to remove file [%s]",entry->d_name);
                file_fail++;
	    } else {
                files++;
            }
	}
    }
    
    stop_time = getMicrosecondTimeStamp();

    if (closedir(dir)) {
	fatal("Unable to close directory [%s]",path);
    }
    if (chdir(oldpath)) {
	fatal("Can't chmod to [%s]",oldpath);
    }
    
    if (level == 0) {
	dt = 1.0e-6 * (double)(stop_time - start_time);
	log_msg("Removed %d files and %d dirs in %10.3f seconds",files,dirs,dt);
        if ((dir_fail + file_fail) > 0) {
            log_msg("Failed on %d dirs and %d files",dir_fail,file_fail);
        }
    }
}

/*
 * this version not only clears the directory but remotes it as well
 */
void remove_dir(char *path)
{
    int recurse = 1;
    struct stat st;
    char startpath[MAXPATHLEN];

    /* save the old path */
    getcwd(startpath,MAXPATHLEN);
    if (my_rank == proc0) {
        log_msg("Entered remove_dir, CWD=[%s] to remove [%s]",startpath,path);
    }

    /* first, check that dir still exists, if not just return */
    if( stat(path,&st) ) {
        if (errno == ENOENT) {
            warning("remove_dir: no such dir [%s]",path);
            return;
        }
    }

    clear_dir(path,recurse);
    if (rmdir(path)) {
	warning("Cant remove directory [%s]",path);
    }
}
   
/* -----------------------------------------------------------
 * This function performs a collective operation over all
 * processes in comm, to create a unique test directory
 * and distribute the path to this directory to all processes
 * in the communicator.
 *
 * ONLY process 0 needs to supply non-null parent and prefix
 * strings.
 *
 * The path to the new dir will be returned in newdir, which
 * is a character array of length strlen.
 *
 * The new dir will have a path of the form:
 *         parent/prefix.XXX
 *
 *
 *  parent = directory into which new subdir will be created
 *           (Ignored unless rank == 0)
 *  prefix = subdir name prefix.  Actual name of subdir created
 *           will begin with this string.
 *           (Ignored unless rank == 0)
 *  newdir = Path to new directory will be stored here
 *  buflen = length of newdir character buffer
 */
void create_unique_dir(char *parent, char *prefix, char* newdir,
		       int buflen, MPI_Comm comm)
{
    struct stat st;
    int newstrlen;

    /* NOTE: proc0 MUST be in the communicator.  Should check this */

    /* first, compute the length of the string needed for the pathname
     * and insure all processes have allocated a large enough buffer
     * to hold the path
     */
    if (my_rank == proc0) {
	newstrlen = strlen(parent) + strlen(prefix) + 6;
    }
    MPI_SAFE(MPI_Bcast(&newstrlen,1,MPI_INT,proc0,comm));
    assert(buflen >= newstrlen);

    /* now have proc 0 try to create a unique subdir of parent
     * with the given prefix
     */
    if (my_rank == proc0) {
	int counter = 0;
        sprintf(newdir,"%s/%s",parent,prefix);
        do {
	    if( stat(newdir,&st) ) {
		/* OK, dir does not exist, lets make one */
		create_path(newdir);
		break;
	    }
            counter++;
	    sprintf(newdir,"%s/%s.%03d",parent,prefix,counter);
        } while (counter < 1000);
	if (counter >= 1000) {
	    fatal("failed to create unique dir in %s with prefix %s",
		  parent,prefix);
	}
    }

    /* now distribute the path name to everyone in comm */
    MPI_SAFE(MPI_Bcast(newdir,newstrlen,MPI_CHAR,proc0,comm));
}


/* alloc memory with given alignment */
void* aligned_alloc(ssize_t size, ssize_t align, void** baseptr)
{
    void *ptr;
    uintptr_t mask = align - 1;

    *baseptr = (void*)malloc(size + align - 1);
    ptr = (void *) (((uintptr_t)(*baseptr) + mask) & ~mask);

    return ptr;
}

/*
 */
int stat_files(char* path, int recurse)
{
    struct stat st;
    uint64_t start_time, stop_time;
    double dt;
    int total;
    DIR *dir;
    struct dirent *entry;
    static int level = 0;
    static int dirs;
    static int reg_files;
    static int links;
    static int fifos;
    static int socks;
    static int chr_devs;
    static int blk_devs;
    static int others;
    char oldpath[MAXPATHLEN];
    char newpath[MAXPATHLEN];


    if (level == 0) {
	/* init counters */
	dirs = reg_files = links = 0;
	fifos = socks = 0;
	chr_devs = blk_devs = 0;
	total = 0;
    }

    /* save the old path */
    getcwd(oldpath,MAXPATHLEN);

    /* first we check that dir exists, then cd to it */
    if( stat(path,&st) ) {
	fatal("Cant stat dir [%s]",path);
    }
    if (!S_ISDIR(st.st_mode)) {
	fatal("[%s] is not a directory",path);
    }
    if (chdir(path)) {
	fatal("Can't chmod to [%s]",path);
    }
    if ( (dir = opendir(".")) == NULL) {
	fatal("Cannot open directory [%s] for walking",path);
    }


    start_time = getMicrosecondTimeStamp();

    while ( (entry = readdir(dir)) != NULL ) {
	if (lstat(entry->d_name,&st)) {
	    fatal("Unable to stat file [%s/%s]",path,entry->d_name);
	}
	if (S_ISREG(st.st_mode) ) {
	    reg_files++;
	} else if (S_ISDIR(st.st_mode)) {
	    if (strcmp(entry->d_name,".") == 0)
		continue;
	    if (strcmp(entry->d_name,"..") == 0)
		continue;
	    dirs++;
	    if (! recurse) continue;
	    sprintf(newpath,"%s/%s",path,entry->d_name);
	    level++;
	    stat_files(newpath,recurse);
	    level--;
	} else if (S_ISFIFO(st.st_mode)) {
	    fifos++;
	} else if (S_ISLNK(st.st_mode)) {
	    links++;
	} else if (S_ISSOCK(st.st_mode)) {
	    socks++;
	} else if (S_ISBLK(st.st_mode)) {
	    blk_devs++;
	} else if (S_ISCHR(st.st_mode)) {
	    chr_devs++;
	} else {
	    fprintf(stderr,"Unknown file %s/%s\n",path,entry->d_name);
	    others++;
	}
    }
    
    stop_time = getMicrosecondTimeStamp();

    if (closedir(dir)) {
	fatal("Unable to close directory [%s]",path);
    }
    if (chdir(oldpath)) {
	fatal("Can't chmod to [%s]",oldpath);
    }

    if (level == 0) {
	printf("In directory: %s\n",path);
	printf("\t%6d directories\n",dirs);
	printf("\t%6d files\n",reg_files);
	printf("\t%6d symbolic links\n",links);
	printf("\t%6d fifos\n",fifos);
	printf("\t%6d sockets\n",socks);
	printf("\t%6d character devices\n",chr_devs);
	printf("\t%6d block devices\n",blk_devs);
	printf("\t%6d other files\n",others);
	total = dirs + reg_files + links + fifos + socks + chr_devs + blk_devs + others;
	dt = 1.0e-6 * (double)(stop_time - start_time);
	printf("Stat-ed %d files in %10.3f seconds\n",total,dt);
    }

    return total;
}


/* -----------------------------------------------------------
 * Convert a char string of the form ddddddX into an integer.
 * where ddd... are digits and X is either k, K, m, M, g, G.
 * k,K represents 2^10 = 1024
 * m,M represents 2^20 = 1024*1024 = 1048576
 * g,G represents 2^30 = 1024*1024*1024 = 1073741824
 *
 * OLD... This was too confusing.  Stick to 2^P
 * k represents 10^3 = 1000
 * K represents 2^10 = 1024
 * m represents 10^6 = 1000000
 * M represents 2^20 = 1024*1024 = 1048576
 * g represents 10^9 = 1000000000
 * G represents 2^30 = 1024*1024*1024 = 1073741824
 *
 * Examples:
 *    64K  => 65536
 &     8g  => 8589934592
 * -----------------------------------------------------------
 */
ssize_t scale_int(const char *s)
{
    ssize_t num;
    char    str[32];
    str[0] = '\0';
    
    sscanf(s,"%lu%1[kKmMgG]",&num,str);

    if (strlen(str) < 1)
	return(num);
    switch (str[0]) {
    case 'k':
    case 'K':
	return (num * KBYTE);
    case 'm':
    case 'M':
	return (num * MBYTE);
    case 'g':
    case 'G':
	return (num * GBYTE);
    default:
	printf("to_int: invalid specifier [%s]\n",str);
	exit(-1);
    }
}

/* -----------------------------------------------------------
 * Timer function
 * -----------------------------------------------------------
 */
uint64_t getMicrosecondTimeStamp(void) {
    uint64_t retval;
    struct timeval tv;
    if (gettimeofday(&tv, NULL)) {
        perror("gettimeofday");
        abort();
    }
    retval = ((uint64_t)tv.tv_sec) * 1000000L + tv.tv_usec;
    return retval;
}

/* -----------------------------------------------------------
 * Print a timestamped message to stdout
 * -----------------------------------------------------------
 */
void log_msg(const char* msg, ...)
{
    time_t gmtime;
    struct tm cur_time;
    va_list args;
    char buf[1000];

    if (my_rank != proc0) {
        /* only proc 0 can write to file handle */
        return;
    }

    va_start(args, msg);
    vsprintf(buf, msg, args);
    va_end(args);

    /* construct name of output file */
    time(&gmtime);
    localtime_r(&gmtime,&cur_time);
    fprintf(ofl,"[%04d-%02d-%04d %02d:%02d:%02d] %s\n",
	    cur_time.tm_year+1900,
	    cur_time.tm_mon+1,cur_time.tm_mday,
	    cur_time.tm_hour,cur_time.tm_min,cur_time.tm_sec,
	    buf);
    fflush(ofl);
}

/* -----------------------------------------------------------
 * Print a timestamped message to stderr
 * -----------------------------------------------------------
 */
void warning(const char* msg, ...)
{
    time_t gmtime;
    struct tm cur_time;
    va_list args;
    char buf[1000];

    va_start(args, msg);
    vsprintf(buf, msg, args);
    va_end(args);

    /* construct name of output file */
    time(&gmtime);
    localtime_r(&gmtime,&cur_time);
    fprintf(stderr,"[%04d-%02d-%04d %02d:%02d:%02d] %s\n",
	    cur_time.tm_year+1900,
	    cur_time.tm_mon+1,cur_time.tm_mday,
	    cur_time.tm_hour,cur_time.tm_min,cur_time.tm_sec,
	    buf);
    fflush(stderr);
}

/* -----------------------------------------------------------
 * Throw in the towel
 * -----------------------------------------------------------
 */
void fatal(const char* msg, ...)
{
    va_list args;
    char buf[1000];
    char newmsg[1000];

    warning("FATAL error on process %d",my_rank);
    sprintf(newmsg,"Proc %d: %s",my_rank,msg);

    va_start(args, msg);
    vsprintf(buf, newmsg, args);
    va_end(args);

    perror(buf);
    exit(-1);
}
