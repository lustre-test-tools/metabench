#!/bin/bash

PKG=rpm
lsb_release -i | grep -q Ubuntu && PKG=deb
bash build-${PKG}.sh
