TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/metabench-1.0.tar.gz metabench-1.0
rpmbuild --define "_topdir $TOPDIR" -bs metabench.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .