#!/bin/bash

export MPICC="mpicc"
cd metabench-*
make -C src
checkinstall --install=no --pkgname=metabench -y -D sh -c "make -C src  && cp src/metabench dictionary /usr/local/bin/"
