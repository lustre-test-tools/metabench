Name:           metabench
Version:        1.0
Release:        1.0%{?dist}
Summary:        Parallel filesystem I/O benchmark
License:        GPL-2.0
Group:          System/Benchmark
Url:            https://github.com/LLNL/ior
Source:         https://github.com/LLNL/ior/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  mpich-devel
Requires:       mpich

%description
Parallel filesystem I/O benchmark

%prep
%setup -q

%build
%{?_mpich_load}
export MPICC="mpicc"
make -C src %{?_smp_mflags}
%{?_mpich_unload}

%install

%{?_mpich_load}

%if 0%{?rhel} < 8
MPI_BIN=%{_bindir}
%endif

mkdir -p %{buildroot}/${MPI_BIN}
cp src/%{name} %{buildroot}/${MPI_BIN}
cp dictionary %{buildroot}/${MPI_BIN}

%{?_mpich_unload}

%files
%if 0%{?rhel} >= 8
%{_libdir}/mpich/bin/metabench
%{_libdir}/mpich/bin/dictionary
%else
%{_bindir}/metabench
%{_bindir}/dictionary
%endif

%changelog
* Sun Aug 6 2018 c17454@cray.com
- Migrate to mpich
- Move to /usr/local/bin
